export default async function seed(wasmB64, jsCode) {
    const b64Js = "data:text/javascript;base64," + btoa(jsCode)
    const b64WasmData = "data:application/wasm;base64," + wasmB64
    const module = await import(/* webpackIgnore: true */ b64Js)
    module.default(b64WasmData)
}