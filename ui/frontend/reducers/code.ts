import { Action, ActionType } from '../actions';

// const DEFAULT: State = `fn main() {
//     println!("Hello, world!");
// }`;

export const DEFAULT: State = `use seed::{prelude::*, *}; 
use seed::virtual_dom::update_el::UpdateEl;
use seed_icons::fa::regular::check_circle;
use crate::Msg;

pub fn view() -> Node<Msg> { 
    div![
        style!{
            "background-color" => "black", 
            "color" => "white", 
            "min-height" => "5rem",
            "line-height" => "5rem",
            "text-align" => "center", 
        },
        green_check(), 
        "Hello, this is your Seed component!"
    ] 
}

fn green_check<T>() -> Node<T> {
    span![ 
        style!{"color" => "green", "margin" => "1rem"},
        check_circle::i()
    ]
}`;

export type State = string;

export default function code(state = DEFAULT, action: Action): State {
  switch (action.type) {
    case ActionType.RequestGistLoad:
      return '';
    case ActionType.GistLoadSucceeded:
      return action.code;

    case ActionType.EditCode:
      return action.code;

    case ActionType.AddMainFunction:
      return `${state}\n\n${DEFAULT}`;

    case ActionType.AddImport:
      return action.code + state;

    case ActionType.EnableFeatureGate:
      return `#![feature(${action.featureGate})]\n${state}`;

    case ActionType.FormatSucceeded:
      return action.code;

    default:
      return state;
  }
}
