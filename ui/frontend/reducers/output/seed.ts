import { Action, ActionType } from '../../actions';
import { finish, start } from './sharedStateManagement';

const DEFAULT: State = {
  wasmCacheKey: null,
  requestsInProgress: 0,
  error: null,
  stderr: null,
  stdout: null,
};

export interface State {
  wasmCacheKey?: string
  requestsInProgress: number
  error?: string
  stdout?: string
  stderr?: string
}

export default function seed(state = DEFAULT, action: Action) {
  switch (action.type) {
    case ActionType.CompileWithSeedRequest:
        return start(DEFAULT, state);
    case ActionType.CompileWithSeedSucceeded: {
        const { stdout = '', stderr = '' } = action;
        const { wasmCacheKey } = action;
        return finish(state, { wasmCacheKey, stdout, stderr });
    }
    case ActionType.CompileWithSeedFailed:
        return finish(state, { error: action.error });
    default:
      return state;
  }
}
