import React from 'react';
import * as selectors from '../selectors';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../reducers';
import SimplePane from './SimplePane';

interface SeedProps { wasmCacheKey?: string }
export default ({ wasmCacheKey }: SeedProps) => {
    const details = useSelector((state: State) => state.output.seed);
    // return <div>yarr</div>
    return <SimplePane {...details} kind="execute"></SimplePane>
}