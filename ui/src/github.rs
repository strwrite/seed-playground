use iron::{
    prelude::*,
};
use crate::GithubAccessTokenReq;
use crate::GithubAccessTokenRes;

use snafu::{ResultExt, Snafu};
use std::{fs, env};

#[tokio::main]
pub async fn gh_access_token(req: &mut iron::Request<'_, '_>) -> iron::IronResult<iron::Response> {

    let res = get_access_token(req).await;

    crate::serialize_to_response(res)
}


async fn get_access_token(req: &mut iron::Request<'_, '_>) -> crate::Result<GithubAccessTokenRes> {

    let body = req.get::<bodyparser::Struct<GithubAccessTokenReq>>()
        .context(crate::Deserialization)?;

    let req = body.ok_or(crate::Error::RequestMissing)?;

    let client_id = env::var("PLAYGROUND_GITHUB_CLIENT_ID").context(crate::GithubClientEnvRead)?;
    let secret_file = env::var("PLAYGROUND_GITHUB_SECRET_FILE").context(crate::GithubClientEnvRead)?;
    let client_secret = fs::read_to_string(secret_file).context(crate::GithubClientSecretFileRead)?;
    let client = reqwest::Client::new();
    let url = format!("https://github.com/login/oauth/access_token?client_id={}&client_secret={}&code={}", client_id, client_secret, req.code);
    let res: crate::GithubAccessTokenRes = client.post(&url)
        .body("")
        .header("Accept", "application/json")
        .send()
        .await.context(crate::GithubAccessTokenRequest)?
        .json()
        .await.context(crate::GithubAccessTokenRequest)?;
    Ok(res)

}