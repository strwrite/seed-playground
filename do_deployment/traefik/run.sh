mkdir -p /root/letsencrypt
cp /etc/letsencrypt/live/api.play-seed.dev/* /root/letsencrypt
( docker stop seed-play-proxy && docker rm seed-play-proxy || echo ignore ) \
 && docker run --network host -d \
    --name seed-play-proxy \
    --restart unless-stopped \
    -v $PWD/traefik:/etc/traefik/  \
    -v $PWD/caddy_data:/data  \
    -v /root/letsencrypt:/tls  \
    traefik:2.3.2
