WEBROOT="/root/seed-playground/ui/frontend/build"
DOMAIN="api.play-seed.dev"

docker run -it --rm --name certbot \
    -v "/etc/letsencrypt:/etc/letsencrypt" \
    -v "${WEBROOT}:${WEBROOT}" \
    certbot/certbot certonly \
        --webroot -d "${DOMAIN}" \
        --webroot-path "${WEBROOT}" \
        --force-renewal
