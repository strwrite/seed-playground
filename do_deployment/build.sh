#!/bin/bash

# This is script, which builds frontend, backend and runs backend

## TODO: CICD - build should happen in CI and deployment triggered as CD

docker run --rm -ti \
    -v /home/seed-playground/ui/frontend:/frontend \
    node:12.13.0-alpine3.10 sh -c '
cd /frontend
yarn
yarn build:production
'

docker run --rm -ti \
    -v /home/seed-playground/ui:/ui \
    rust:1.47.0 bash -c '
cd /ui
mkdir -p .cargo
cargo vendor --no-delete --versioned-dirs ./cargo-deps > .cargo/config.toml
cargo build --release
' && PLAYGROUND_UI_ROOT="/home/seed-playground/ui/frontend/build" \
     PLAYGROUND_GITHUB_TOKEN="" \
     /home/seed-playground/ui/target/release/ui
