#!/bin/bash

# Caddy is a variant for reverse proxy, which can very easily 
# terminate TLS and provide compression. 
# However v2 of Caddy cannot do rate limiting, that's why
# currently Caddy is changed to much harder to configure, 
# but more powerful traefik 

( docker stop seed-play-proxy && docker rm seed-play-proxy || echo ignore ) \
 && docker run --network host -d \
    --name seed-play-proxy \
    --restart unless-stopped \
    -v $PWD/Caddyfile:/etc/caddy/Caddyfile  \
    -v $PWD/caddy_data:/data  \
    -v /home/tls:/tls  \
    caddy
