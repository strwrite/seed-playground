#!/bin/bash

mkdir /root/seed-playground/ui
systemctl stop playground
SEED_PLAY_BIN=/root/seed-playground/do_deployment/playground
curl -Lo "${SEED_PLAY_BIN}" \
    'https://gitlab.com/strwrite/seed-playground/-/jobs/artifacts/master/raw/ui/target/release/ui?job=build'

chmod +x "${SEED_PLAY_BIN}"
cp playground.service /etc/systemd/system/playground.service
systemctl enable playground
systemctl restart playground