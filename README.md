# Seed Playground

This is an experimental project, which tries to provide 
a proof-of-concept playground to demo Seed framework.

It's derived from [Rust Plaground](https://github.com/integer32llc/rust-playground).