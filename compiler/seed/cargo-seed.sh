#!/usr/bin/env bash

set -eu

shift # Ignore "wasm"
args=()
while (( "$#" )); do
    if [[ "$1" == "--" ]] ; then
        : # Ignore
    elif [[ "$1" == "-o" ]] ; then
        shift
        output="$1"
    else
        args+="$1"
    fi

    shift
done

wasm-pack build --target web --out-name package --dev

cat pkg/package_bg.wasm | base64 > "${output}.b64-wasm"
cat pkg/package.js > "${output}.js"
