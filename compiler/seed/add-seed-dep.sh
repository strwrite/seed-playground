#!/bin/bash

cat << EOF >> Cargo.toml

[lib]
crate-type = ["cdylib"]

[dependencies.seed]
package = "seed"
git = "https://github.com/seed-rs/seed"
rev = "b25cf59"

EOF



