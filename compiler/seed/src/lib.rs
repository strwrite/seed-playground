use seed::{prelude::*};
mod component;


fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model::default()
}

type Model = i32;
pub enum Msg {}

fn update(_msg: Msg, _model: &mut Model, _: &mut impl Orders<Msg>) {
}

fn view(_model: &Model) -> Node<Msg> {
    component::view()
}

#[wasm_bindgen(start)]
pub fn start() {
    App::start("app", init, update, view);
}
